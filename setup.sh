# 環境で利用できるpythonのバージョン
PYTHON_VERSION=3.7 # >= 3.7 by yolov5

# 必要物のインストール
sudo apt install ffmpeg
sudo apt install python${PYTHON_VERSION}-venv


# 仮想環境の作成
python${PYTHON_VERSION} -m venv venv
source venv/bin/activate
pip install -r requirements.txt

# yolov5のclone
git clone https://github.com/ultralytics/yolov5.git
pip install -r yolov5/requirements.txt
