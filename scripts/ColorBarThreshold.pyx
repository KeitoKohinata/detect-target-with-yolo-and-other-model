"""
コンパイル方法
cd scripts; python setup.py build_ext -i; cd ..
"""

import time
import pathlib
import cv2
import numpy as np
import os 
import yaml
import csv
import sys
cimport numpy as cnp


def calcIou(a: list, b: list, int_flag: bool = False):
    """
    IOUを計算する
    Args
        `a`:  float もしくは int の bounding box [xmin, ymin, xmax, ymax]
        `b`:  上に同じ
        `int_flag` : a,b が intである場合
    Return
        (`iou`:float, `intersection`: float or int, `union`: float or int)
    """
    c = 0
    if int_flag:
        c = 1
    ax_mn, ay_mn, ax_mx, ay_mx = a[0], a[1], a[2], a[3]
    bx_mn, by_mn, bx_mx, by_mx = b[0], b[1], b[2], b[3]

    a_area = (ax_mx - ax_mn + c) * (ay_mx - ay_mn + c)
    b_area = (bx_mx - bx_mn + c) * (by_mx - by_mn + c)

    abx_mn = max(ax_mn, bx_mn)
    aby_mn = max(ay_mn, by_mn)
    abx_mx = min(ax_mx, bx_mx)
    aby_mx = min(ay_mx, by_mx)
    w = max(0, abx_mx - abx_mn + c)
    h = max(0, aby_mx - aby_mn + c)
    intersect = w*h
    union = a_area + b_area - intersect
    iou = intersect / union
    return iou,intersect,union

# def judgeOverlap(box1: list, box2: list, intersect_th = 0.8) -> int:
#     _, intersect, _ = calcIou(box1, box2)
#     area1 = (box1[2]-box1[0])*(box1[3]-box1[1])
#     area2 = (box2[2]-box2[0])*(box2[3]-box2[1])
#     if area1*intersect_th <= intersect:
#         return 2
#     elif area2*intersect_th <= intersect:
#         return 1
#     else:
#         return 0

def calcArea(box):
    return (box[3]-box[1])*(box[2]-box[0])

def calcAreai(box):
    return (box[3]-box[1]+1)*(box[2]-box[0]+1)

def GaussianBlur(img):
    kernel_size = 11
    return cv2.GaussianBlur(img, (kernel_size,kernel_size), sigmaX=0)

class ColorBarThreshold():
    def __init__(
        self,
        enemy_color = "blue", 
        class_file = "yaml/class.yaml",
        config_file = "yaml/color_bar_threshold.yaml",
        dataset_dir = "videos/blue_bright_mp4_cropped/", 
        ):
        """
        Args
            `enemy_color` : 敵色
            `class_file` : yoloの学習に使ったyamlファイル．nameがあればいい
            `config_file` : thresholdを保存したyamlファイル
            `dataset_dir` : images/ と labels/ があるフォルダ．thresholdの計算に使う． labels/ は， yolo で images/ を検出した結果のフォルダ．
        """
        self.enemy_color = enemy_color
        self.class_file = class_file
        self.config_file = config_file
        self.dataset_dir = dataset_dir

        # enemy_colorに対するbgrのインデックスについて
        self.assignColorIndex()

        # threshold調整に関するハイパーパラメータ
        self.opposite_color_rate = 0.99
        self.green_rate = 1

        # すでに調整されたスレッショルド値の読み込み
        self.readConfig()
        
    
    def prepare(self):
        """
        fit に必要なデータを用意する．

        Return
            `self.names` : labels/label.txt の label
            `self.armors` : 装甲板のboundingbox (float : 0~1)
            `self.bar` : カラーバーのboundingbox (float : 0~1)
            `self.imgs` : 画像からカラーバー部分を切り取った画像
            `self.acrds` : カラーバーの絶対座標(lx,ly) (int : img.shape)
            `self.rcrds` : カラーバーの総体座標boundingbox (int : img.shape)
        """
        self.images_dir = self.dataset_dir + "/images/"
        self.labels_dir = self.dataset_dir + "/labels/"

        # 物体のclass番号の読み込み
        self.readClassNum()
        # ラベルの読み込み
        self.readLabels()
        # 誤検出と思われるカラーバーを除外する
        self.excludeMisdetected(self.bars)
        # カラーバー部分を切り取る
        self.imgs, acrds, self.rcrds = self.trimImage(self.bars) 
        return self.names, self.armors, self.bars, self.imgs, acrds, self.rcrds



    def fit(
        self,
        int start = 30,
        int stop = 256,
        int step = 5,
        ):
        """
        カラーバーを認識しやすい self.threshold を決定する．2値化後のカラーバーの boundingbox と label の boundingbox との iou の平均が最大となるようにする． 

        Args
            `start` : 調整におけるthresholdの初期値
            `stop` : 調整におけるthresholdの最終値
            `step` : 調整におけるthresholdのステップ
        """
        threshold_best = start
        self.best_ave_iou = -1
        # kernel_size = 5
        # kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(kernel_size, kernel_size)) 
        for self.threshold in range(start,stop,step):
            c = 0
            sum_iou = 0
            for imgs, bars_ans in zip(self.imgs, self.rcrds):
                for img, bar_ans in zip(imgs, bars_ans):
                    c += 1
                    img_bin =self.inRange(img)
                    # img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
                    contours, _ = cv2.findContours(img_bin, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                    if len(contours) == 0:
                        continue
                    contour_max = max(contours, key=lambda x:cv2.contourArea(x))
                    lx,ly,w,h = cv2.boundingRect(contour_max)
                    bar = [lx,ly,lx+w-1,ly+h-1]

                    # 重み
                    weight = 1/calcAreai(bar_ans)
                    # weight = 1

                    sum_iou += weight * calcIou(bar, bar_ans, True)[0]

            ave_iou = sum_iou/c
            # print("th, ave", self.threshold, ave_iou)
            if ave_iou > self.best_ave_iou:
                self.best_ave_iou = ave_iou
                threshold_best = self.threshold
        print("fit_best", threshold_best)
        self.threshold = threshold_best


    def assignColorIndex(self):
        if self.enemy_color == "blue":
            self.enemy_color_index = 0 
            self.ally_color_index = 2 
        elif self.enemy_color == "red" :
            self.enemy_color_index = 2
            self.ally_color_index = 0
        else :
            print("%s: enemy_color must be red or blue. %s"%(__file__, self.enemy_color))
            sys.exit(1)


    def readConfig(self):
        with open(self.config_file, "r") as yml:
            self.threshold_cfg = yaml.safe_load(yml)
            self.threshold = self.threshold_cfg[self.enemy_color]


    def readClassNum(self):
        with open(self.class_file, "r") as yml:
            dataset_cfg = yaml.safe_load(yml)
            self.bar_class_number = dataset_cfg["names"].index(self.enemy_color+"-bar")
            self.armor_class_number = dataset_cfg["names"].index(self.enemy_color)

        
    def readLabels(self) -> tuple:
        self.names = []
        self.armors = []
        self.bars = []
        for label_file in sorted(os.listdir(self.labels_dir)):
            with open(self.labels_dir+label_file, "r") as f:
                reader = csv.reader(f, delimiter=" ")
                labels = np.array([row for row in reader], dtype=np.float64)
            label_file = pathlib.PurePath(label_file).stem
            self.names.append(label_file)
            armors = []
            bars = []
            for label in labels:
                cx,cy,w,h = label[1:]
                lx = cx-w/2
                ly = cy-h/2
                rx = cx+w/2
                ry = cy+h/2
                box = [lx,ly,rx,ry]
                if label[0] == self.bar_class_number:
                    bars.append(box)
                if label[0] == self.armor_class_number:
                    armors.append(box)
            self.bars.append(bars)
            self.armors.append(armors)


    def excludeMisdetected(self, boxes_: list):
        """
        yoloの誤検出を取り除く．（作成理由 : yoloがなんかでかめのカラーバーを検出してしまうことがあるため）

        Args
            `labels` : self.armors と self.bars のどちらか
        """
        for boxes in boxes_:
            pop_list = set()
            for i in range(len(boxes)-1):
                for j in range(i+1, len(boxes)):
                    # 誤検出の除外
                    intersect_th = 0.5
                    area_th = 2
                    _, intersect, _ = calcIou(boxes[i], boxes[j])
                    area1 = calcArea(boxes[i])
                    area2 = calcArea(boxes[j])
                    if area1*intersect_th <= intersect and area2/area1 >= area_th :
                        pop_list.add(j)
                    elif area2*intersect_th <= intersect and area1/area2 >= area_th:
                        pop_list.add(i)
            pop_list = sorted(pop_list)
            for i in range(len(pop_list)):
                boxes.pop(pop_list[i]-i)


    def trimImage(self, boxes_: list):
        """
        画像から検出部分を切り取る．

        Args
            `labels_dic` : self.armors か self.bars
        Return
            `imgs_` : {"label":[img, ...], ...}
            `acrds_` : {"label" : [絶対座標(lx,ly), ...], ...}
            `rcrds_` : {"label" : [相対座標のbox, ...], ...}
        """
        imgs_ = []
        acrds_ = []
        rcrds_ = []
        for label, boxes in zip(self.names, boxes_):
            imgs = []
            acrds = []
            rcrds = []
            # 画像読み込み
            img_file = self.images_dir + label + ".png"
            img = cv2.imread(img_file)
            # ぼかし
            img = GaussianBlur(img)
            # 切り取り
            margin = 5
            for (lx,ly,rx,ry) in boxes:
                # 整数値化
                lx = int(lx*img.shape[1])
                ly = int(ly*img.shape[0])
                rx = int(rx*img.shape[1])
                ry = int(ry*img.shape[0])

                # 切り取り余白の設定
                lx_m = max(0, lx-margin)
                ly_m = max(0, ly-margin)
                rx_m = min(img.shape[1], rx+margin)
                ry_m = min(img.shape[0], ry+margin)

                acrds.append([lx_m, ly_m])
                
                imgs.append(img[ly_m:ry_m,lx_m:rx_m].copy())

                # 切り取り後の相対座標
                lx_r = lx - lx_m
                ly_r = ly - ly_m
                rx_r = lx_r + rx - lx
                ry_r = ly_r + ry - ly
                
                rcrds.append([lx_r, ly_r, rx_r, ry_r])

                # 表示 : デバッグ用
                # print("m", lx_m, ly_m, rx_m, ry_m)
                # print("r", lx_r, ly_r, rx_r, ry_r)
                # cv2.rectangle(imgs_[label][-1], [lx_r,ly_r], [rx_r,ry_r], (0,255,0), 1, lineType=cv2.LINE_AA)
                # cv2.imshow("img", imgs_[label][-1])
                # key = cv2.waitKey()
                # cv2.destroyWindow("img")
                # if key == ord("q"):
                #     quit()
            imgs_.append(imgs)
            acrds_.append(acrds)
            rcrds_.append(rcrds)
        return imgs_, acrds_, rcrds_


    def save(self, yaml_file=None):
        if yaml_file is None:
            yaml_file = self.config_file
        with open(self.config_file, "w") as yml:
            self.threshold_cfg[self.enemy_color] = self.threshold
            yaml.dump(self.threshold_cfg, yml)

    def inRange(self, 
        cnp.ndarray[cnp.uint8_t, ndim=3] img, 
        ):
        """
        bgr 画像を self.threshold で2値化する．

        Args
            `img` : bgr 画像
        """
        # time_start = time.time()

        cdef :
            int h
            int w
            int th
            float gr
            float ocr
            int eci
            int aci
            int e
            int g
            int a
        h = img.shape[0]
        w = img.shape[1]
        th = self.threshold
        gr = self.green_rate
        ocr = self.opposite_color_rate
        eci = self.enemy_color_index
        aci = self.ally_color_index

        img_bin = np.zeros((h,w), np.uint8)
        for i in range(h):
            for j in range(w):
                e = img[i,j,eci]
                g = img[i,j,1]
                a = img[i,j,aci]
                if e >= th and g <= e*gr and a <= e*ocr:
                    img_bin[i,j] = 255

        return img_bin
    
    def checkThreshold(self):
        for imgs in self.imgs:
            for img in imgs:
                img_bin = self.inRange(img)
                cv2.imshow("img", img_bin)
                key = cv2.waitKey(0)
                cv2.destroyAllWindows()
                if key == ord("q"):
                    return


def main():
    cbt = ColorBarThreshold()
    cbt.prepare()
    cbt.fit()
    print(cbt.threshold)
    cbt.save()
    cbt.checkThreshold()

if __name__=="__main__":
    main()