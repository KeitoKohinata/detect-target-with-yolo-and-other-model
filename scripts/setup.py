"""
コンパイル方法
cd scripts; python setup.py build_ext -i; cd ..
"""

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

import numpy as np

library_name = "ColorBarThreshold"
source_files = ["ColorBarThreshold.pyx"]
include_dirs = [np.get_include()]

setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = [Extension(library_name, source_files, include_dirs)],
)