# %%
####################################
# yoloによる装甲板及びカラーバーの検出が正しく行われていることを確認する．
####################################

# %%
import pathlib
import cv2
import numpy as np
import os 
import yaml
import csv


def calc_iou(a, b):
    ax_mn, ay_mn, ax_mx, ay_mx = a[0], a[1], a[2], a[3]
    bx_mn, by_mn, bx_mx, by_mx = b[0], b[1], b[2], b[3]

    a_area = (ax_mx - ax_mn) * (ay_mx - ay_mn)
    b_area = (bx_mx - bx_mn) * (by_mx - by_mn)

    abx_mn = max(ax_mn, bx_mn)
    aby_mn = max(ay_mn, by_mn)
    abx_mx = min(ax_mx, bx_mx)
    aby_mx = min(ay_mx, by_mx)
    w = max(0, abx_mx - abx_mn)
    h = max(0, aby_mx - aby_mn)
    intersect = w*h
    union = a_area + b_area - intersect
    iou = intersect / union
    return iou,intersect,union

def judge_bar_fp(bar1, bar2):
    intersect_threshold = 0.8
    _, intersect, _ = calc_iou(bar1, bar2)
    area1 = (bar1[2]-bar1[0])*(bar1[3]-bar1[1])
    area2 = (bar2[2]-bar2[0])*(bar2[3]-bar2[1])
    if area1*intersect_threshold <= intersect :
        return 2
    elif area2*intersect_threshold <= intersect:
        return 1
    else:
        return 0


# 検出する装甲板LED色
enemy_color = "blue"

# データセットのパス
# 1動画から切り出しされた画像とラベルが保存されているものと過程
dataset_dir = "../USB_CAM_60FPS_MOV/cropped/blue_bright/"
# 
yaml_file = "../yaml/class.yaml"
images_dir = dataset_dir + "images/"
labels_dir = dataset_dir + "labels/"

# データセット情報取得
with open(yaml_file, "r") as yml:
    dataset_config = yaml.safe_load(yml)
bar_class_number = dataset_config["names"].index(enemy_color+"-bar")
armor_class_number = dataset_config["names"].index(enemy_color)

# yoloが検出したカラーバーと装甲板を表示する．
image_files = sorted(os.listdir(images_dir))
label_files = sorted(os.listdir(labels_dir))
key = 0
file_index = 0
while key != ord("q"):
    image_file = image_files[file_index]
    label_file = pathlib.PurePath(image_file).stem + ".txt"

    image_file = images_dir + image_file

    if not label_file in label_files:
        labels = np.array([[-1]])
    else:
        with open(labels_dir+label_file, "r") as f:
            reader = csv.reader(f, delimiter=" ")
            labels = np.array([row for row in reader], dtype=np.float64)

    img = cv2.imread(image_file)

    detected_bars = []
    
    # for i in np.where(
    #     (labels[:,0]==bar_class_number)
    #     )[0]:
    #     cx,cy,w,h = labels[i,1:]
    #     lx = int((cx-w/2)*img.shape[1]+0.5)
    #     ly = int((cy-h/2)*img.shape[0]+0.5)
    #     rx = int((cx+w/2)*img.shape[1]+0.5)
    #     ry = int((cy+h/2)*img.shape[0]+0.5)
    #     bar = [lx,ly,rx,ry]

    #     detected_bars.append(bar)

        # 誤検出の除外
        # append_flag = True
        # pop_list = []
        # for j in range(len(detected_bars)):
        #     res = judge_bar_fp(bar, detected_bars[j])
        #     if res==1:
        #         append_flag = False
        #     elif res==2:
        #         pop_list.append(j)
        # for i in range(len(pop_list)):
        #     detected_bars.pop(pop_list[i]-i)
        # if append_flag == True:
        #     detected_bars.append(bar)

    detected_armors = []
    for i in np.where(
        (labels[:,0]==armor_class_number) 
        )[0]:
        cx,cy,w,h = labels[i,1:]
        lx = int((cx-w/2)*img.shape[1]+0.5)
        ly = int((cy-h/2)*img.shape[0]+0.5)
        rx = int((cx+w/2)*img.shape[1]+0.5)
        ry = int((cy+h/2)*img.shape[0]+0.5)
        armor = [lx,ly,rx,ry]
        detected_armors.append(armor)

    for bar in detected_bars:
        cv2.rectangle(img, (bar[0],bar[1]), (bar[2],bar[3]), (0,255,0),thickness=2, lineType=cv2.LINE_AA)
    
    for armor in detected_armors:
        cv2.rectangle(img, (armor[0],armor[1]), (armor[2],armor[3]), (0,255,0),thickness=1, lineType=cv2.LINE_AA)

    cv2.putText(img, image_file, (10,20), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255,255,255), 1, cv2.LINE_AA)
    cv2.imshow("img",img)
    key = cv2.waitKey()
    if key == ord("j"):
        file_index -= 1
    elif key == ord("l"):
        file_index += 1
    file_index = (file_index+len(image_files))%len(image_files)
    
cv2.destroyAllWindows()


# %%
from ColorBarThreshold import ColorBarThreshold, calcArea, calcIou
import pathlib
import cv2
import numpy as np
import os 
import yaml
import csv

