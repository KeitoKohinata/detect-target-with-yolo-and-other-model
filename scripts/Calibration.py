# example
# python scripts/calibration_test.py --video videos/blue_bright.mp4 --enemy blue --calib; python scripts/calibration_test.py --video videos/blue_bright.mp4 --enemy blue --test --no-yolo 

import argparse
from ArmorClassifier import ArmorClassifier 
from ColorBarThreshold import ColorBarThreshold, GaussianBlur

import sys
import os
import subprocess
import pathlib
import cv2
import time


class Calibration():
    def __init__(self):
        """
        svc と ColorBarThresholdによる画像2値化のためのスレッショルドを決める
        """ 
        # オプション取得
        self.parse_opt()
        # print("first opt\n", opt, "\n")

        # ffmpeg
        if not self.opt["no_crop"]:
            self.crop()
        # yolo
        if not self.opt["no_yolo"]:
            self.yolo()

        # cbtの準備
        self.cbt = ColorBarThreshold(enemy_color = self.opt["enemy"], dataset_dir = self.opt["cropped"],config_file = self.opt["thresh"])
        self.names, self.armors, self.bars, self.imgs, self.acrds, _ = self.cbt.prepare()

        # clfの準備
        self.clf = ArmorClassifier(inRange = self.cbt.inRange, clf_file=self.opt["clf"], scl_file=self.opt["scl"])
        self.clf.prepare(self.opt["cropped"], self.names, self.armors, self.bars, self.imgs, self.acrds, caution=False)

        # cailbration
        if self.opt["calib"]:
            self.calibration()
        # test
        if self.opt["test"]:
            self.test()
            
        # opt の表示
        print("\nopt: \n", self.opt)
        # 終了表示
        print("\n%s: successfully completed"%(__file__))
        

    def crop(self):
        # 前の cropped フォルダを消去
        subprocess.run("rm -rf {cropped}".format(**self.opt), check=True, shell=True)
        # images フォルダの作成
        subprocess.run("mkdir -p {images}".format(**self.opt), check=True, shell=True)

        # ffmpeg で crop
        self.opt["percent"] = "%"
        subprocess.run("ffmpeg -i {video} -r {crop_rate} -q:v 5 -vcodec {vcodec} {images}/{percent}04d.{vcodec} > /dev/null".format(**self.opt), check=True, shell=True)
        

    def yolo(self):
        # yolo で detect
        subprocess.run("cd yolov5; python detect.py --weights ../pt/{pt} --imgsz 600 800 --source ../{images} --save-txt --project ../{cropped} --name . --exist-ok > /dev/null; cd ..".format(**self.opt), check=True, shell=True)

        # 結果の .png を移動
        subprocess.run("cd {cropped}; mkdir yolo_images; mv *.png yolo_images/".format(**self.opt), check=True, shell=True)
        

    def calibration(self):
        # ColorBarThresholdの保存
        self.cbt.fit(self.opt["start"], self.opt["stop"], self.opt["step"])
        self.cbt.save()

        # ArmorClassfierの保存
        self.clf.fit()
        self.clf.save()
        

    def test(self):
        # ArmorClassfier の test
        self.clf.test()

        if not (self.opt["save_video"] or self.opt["play"]) :
            return 
        
        # 座標描画後の動画の保存先
        save_fold = "videos_after_detection/"
        save_file = save_fold + os.path.basename(self.opt["video"])
        subprocess.run("mkdir -p {save_fold}".format(save_fold=save_fold), check=True, shell=True)
        # capの用意
        cap = cv2.VideoCapture(self.opt["video"])
        fps = int(cap.get(cv2.CAP_PROP_FPS))
        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fmt = cv2.VideoWriter_fourcc("m","p","4","v")
        # writerの用意
        writer = cv2.VideoWriter(save_file, fmt, fps, (width, height))
        # 再生および保存の実行
        wait = 1000/fps
        time_sum, count = 0, 0
        ret, img = cap.read()
        # s = time.time()
        while ret:
            # detect と 実行時間計測
            s = time.time()
            centers,_,probs = self.clf.detect(img)
            e = time.time()
            time_sum += e-s
            count += 1
            # img に描画
            img = GaussianBlur(img)
            for c, p in zip(centers, probs):
                cv2.circle(img, c, 5, (0,255,0), 3, cv2.LINE_AA)
                c += [-10,-10]
                cv2.putText(img, "%.2lf"%(p), c, cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), 2)
            # play なら再生
            if self.opt["play"]:
                e = time.time()
                cv2.imshow("detect", img)
                if e-s >= wait:
                    key = cv2.waitKey(1)
                else:
                    key = cv2.waitKey(int(wait-(e-s)))
                s = time.time()
                if key == ord("q"):
                    break
            # save_video なら保存
            if self.opt["save_video"]:
                writer.write(img)
                pass
            ret, img = cap.read()

        cv2.destroyAllWindows()
        writer.release()
        print("FPS:", count/time_sum)


    def parse_opt(self):
        parser = argparse.ArgumentParser()

        # 確実に必要
        parser.add_argument("--video", type=str, help="specify video required by default")
        parser.add_argument("--enemy", type=str, help="enemy color required by default")

        # 使いたいときだけ
        parser.add_argument("--calib", action="store_true", help="")
        parser.add_argument("--test", action="store_true", help="")
        parser.add_argument("--thresh", type=str, default="yaml/color_bar_threshold.yaml", help="specify yaml file for ColorBarThreshold")
        parser.add_argument("--clf", type=str, default="classifier/clf.pkl", help="If calibration, this is a saved file. If test, this is a used file.")
        parser.add_argument("--scl", type=str, default="scaler/scl.pkl", help="If calibration, this is a saved file. If test, this is a used file.")
        parser.add_argument("--start", type=int, default=30, help="start used in ColorBarThreshold")
        parser.add_argument("--stop", type=int, default=256, help="stop used in ColorBarThreshold")
        parser.add_argument("--step", type=int, default=5, help="step used in ColorBarThreshold")
        parser.add_argument("--crop-rate", type=int, default=5, help="crop rate used by ffmpeg -r")
        parser.add_argument("--cropped", type=str, default="", help="folder where [calib or test]_cropped are created")
        parser.add_argument("--vcodec", type=str, default="png", help="video codec used by ffmpec -vcodec" )
        parser.add_argument("--pt", type=str, default="armors_bars.pt", help="parameter file used in yolo")
        parser.add_argument("--no-yolo", action="store_true", help="if no yolo, [no crop] become True")
        parser.add_argument("--no-crop", action="store_true", help="")
        parser.add_argument("--save-video", action="store_true", help="save video after detection")
        parser.add_argument("--play", action="store_true", help="play video after detection")

        # TODO : 使え
        # parser.add_argument("--mistake", action="store_true", help="display mistake images")

        # 辞書化
        opt = vars(parser.parse_args())

        # エラー処理
        if not os.path.isfile(opt["video"]):
            print("%s: specify existing file"%(__file__), file=sys.stderr)
            sys.exit(1)
        if opt["enemy"] != "red" and opt["enemy"] != "blue":
            print("%s: select red or blue as the enemy color"%(__file__), file=sys.stderr)
            sys.exit(1)

        # no_yolo の場合は crop もしない
        if opt["no_yolo"]:
            opt["no_crop"] = True

        # cropped の場所に関する前処理
        if opt["cropped"] == "":
            opt["cropped"] = opt["video"].replace(".","_")+"_cropped/"
        else :
            cropped = os.path.basename(opt["video"]).replace(".","_")+"_cropped/"
            opt["cropped"] = opt["cropped"] + "/" + cropped
        opt["images"] = "{cropped}/images/".format(**opt)
        opt["labels"] = "{cropped}/labels/".format(**opt)

        self.opt = opt


if __name__=="__main__":
    Calibration()