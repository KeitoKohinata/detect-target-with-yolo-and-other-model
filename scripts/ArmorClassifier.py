
from time import time
from sklearn.preprocessing import StandardScaler 
from sklearn.svm import SVC
from sklearn import datasets, metrics
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from ColorBarThreshold import calcIou, calcArea, ColorBarThreshold, GaussianBlur
import cv2
import numpy as np
import pickle
import os
import sys

def dilate(img):
    kernel_size = 7
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernel_size,kernel_size))
    return cv2.dilate(img, kernel, iterations=1)

def fitEllipse(contour, dx, dy, img_height):
    if len(contour) <= 4:
        return [0,0,0,0,0]
    (cx,cy),(h,w),deg = cv2.fitEllipse(contour)
    # 値としてとんでもなくでかい feature が含まれることがあるため．原因は知らん．
    for f in [cx,cy,h,w]:
        if np.abs(f) > img_height*10 or np.isnan(f):
            print("fitEllipse h=%s"%(img_height), cx,cy,h,w,deg)
            return [0,0,0,0,0]
    if np.isnan(deg):
            print("fitEllipse h=%s"%(img_height), cx,cy,h,w,deg)
            return [0,0,0,0,0]
    return cx+dx,cy+dy,h,w,deg
                
    

class ArmorClassifier():

    def __init__(self, 
        inRange, 
        clf_file: str = "classifier/clf.pkl",
        scl_file: str = "classifier/scl.pkl",
        ):
        """
        bgr 画像から装甲板を検出する svc の作成，保存する．

        Args
            `inRange` : bgr 画像を2値化する関数 func(img)\n
            `clf_file` : 以前に作成した svc  ．\n
            `scl_file` : 以前に作成した scaler ．\n
        """
        if clf_file is not None and os.path.exists(clf_file):
            with open(clf_file, mode='rb') as f:
                self.clf = pickle.load(f)
        if scl_file is not None and os.path.exists(scl_file): 
            with open(scl_file, mode='rb') as f:
                self.scl = pickle.load(f)

        self.inRange = inRange
        self.scl_file = scl_file
        self.clf_file = clf_file

    def prepare(self, dataset_dir, names, armors, bars, imgs, acrds, caution=False,):
        """
        学習，結果の分析，テストに用いるデータセットを作成する．

        Args
            `dataset_dir` : images/ と labels/ があるフォルダ．\n
            `names` : labels/label.txt の label\n
            `armors` : 装甲板のboundingbox (float : 0~1)\n
            `bar` : カラーバーのboundingbox (float : 0~1)\n
            `imgs` : 画像からカラーバー部分を切り取った画像\n
            `acrds` : カラーバーの絶対座標(lx,ly) (int : img.shape)\n
            `caution` : 装甲板内のカラーバーが2本でない時に警告するかどうか\n
            詳細は ColorBarThreshold.py 参照
        """
        self.dataset_dir = dataset_dir
        self.names = names
        self.armors = armors
        self.bars = bars
        self.imgs = imgs
        self.acrds = acrds
        self.caution = caution
        self.makePairs()
        self.findFeatures()
        self.makeDataset()


    def makePairs(self):
        """
        ある装甲板内のカラーバーが2つの時，ペアを作る．
        """
        intersect_th = 0.8
        self.pairs = []
        for name, armors, bars in zip(self.names, self.armors, self.bars):
            pairs = []
            for armor in armors:
                pair = []
                # area_armor = calcArea(armor)
                for i, bar in enumerate(bars):
                    area_bar = calcArea(bar)
                    _, intersection, _ = calcIou(armor, bar)
                    if area_bar * intersect_th <= intersection:
                        pair.append(i)
                if len(pair) == 2:
                    pairs.append(pair)
                elif self.caution and (len(pair) != 2):
                    print("label=%s armor=%s len(pair)=%d"%(name, armor, len(pair)))
            self.pairs.append(pairs)

    def findFeatures(self):
        """
        fitEllipse の結果を特徴量として保存する．
        """
        self.features = []
        for name, acrds, imgs in zip(self.names, self.acrds, self.imgs):
            features = []
            for acrd, img in zip(acrds, imgs):
                dx,dy = acrd
                # img に GaussianBlurは既ににかかっている．
                img = self.inRange(img)
                img = dilate(img)
                contours, _ = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                if len(contours) == 0:
                    features.append(np.zeros(5))
                    continue
                contour_max = max(contours, key=lambda x:cv2.contourArea(x))
                feature = fitEllipse(contour_max,dx,dy,img.shape[1])
                features.append(feature)
            self.features.append(features)

    def makeDataset(self):
        self.data = []
        self.targets = []
        self.indexes = []
        for k, (pairs, features) in enumerate(zip(self.pairs, self.features)):
            for i in range(len(features)):
                cx1,cy1,*other1 = features[i]
                for j in range(i+1, len(features)):
                    cx2,cy2,*other2 = features[j]
                    self.data.append([[cx1-cx2,cy1-cy2,*other1,*other2],
                             [cx2-cx1,cy2-cy1,*other2,*other1]])
                    self.indexes += [[k,i,j],[k,j,i]]
                    if ([i,j] in pairs) and (0 not in features[i][:4]) and (0 not in features[j][:4]):
                        self.targets.append([1,1]) 
                    else :
                        self.targets.append([0,0]) 
        self.data = np.array(self.data)
        self.targets = np.array(self.targets)
        self.data_1d = np.reshape(self.data, (len(self.data)*len(self.data[0]), -1))
        self.targets_1d = np.reshape(self.targets, -1)

    def fitAndTest(self):
        """
        1つのデータセットを分割して学習とテストをする
        """
        # データ分割
        d_train, d_test, t_train, t_test = train_test_split(self.data, self.targets, test_size=0.2, random_state=0)
        d_train = np.reshape(d_train, (len(d_train)*len(self.data[0]), -1))
        d_test  = np.reshape(d_test, (len(d_test)*len(self.data[0]), -1))
        t_train = np.reshape(t_train, -1)
        t_test  = np.reshape(t_test, -1)
        # 標準化
        self.scl = StandardScaler()
        self.scl.fit(d_train)
        d_train_std = self.scl.transform(d_train)
        data_std = self.scl.transform(self.data_1d)
        d_test_std = self.scl.transform(d_test)
        # 学習
        self.clf = SVC(probability=True)
        self.clf.fit(d_train_std, t_train)
        # 結果表示
        print('train score: ', self.clf.score(d_train_std, t_train))
        # 詳細な結果表示
        self.__test(d_test, t_test)
        
    def fit(self):
        # 標準化
        self.scl = StandardScaler()
        self.scl.fit(self.data_1d)
        data_std = self.scl.transform(self.data_1d)
        # 学習
        self.clf = SVC(probability=True)
        self.clf.fit(data_std, self.targets_1d)
        # 結果
        # print('score : ', self.clf.score(data_std, self.targets_1d))

    def test(self):
        """
        データに対して作成したモデルを使ってテストする
        """
        self.__test(self.data_1d, self.targets_1d)

    def __test(self, data, targets):
        # 標準化
        d_test_std = self.scl.transform(data)
        data_std = self.scl.transform(self.data_1d)
        # 結果
        print('test score : ', self.clf.score(d_test_std, targets))

        # 予測
        d_test_predict = self.clf.predict(d_test_std)
        self.data_predict = self.clf.predict(data_std)

        # decision_function値の確認
        # d_test_decision = self.clf.decision_function(d_test_std)
        # print("decision_function")
        # for a in zip(d_test_predict, d_test_decision):
        #     print(a)
        
        # precisionとrecallの表示
        precision = metrics.precision_score(targets,d_test_predict, average=None, zero_division=0)
        print("precision : ", precision[1])
        recall = metrics.recall_score(targets, d_test_predict, average=None)
        print("recall : ", recall[1])
        
        # 混同行列を表示．横軸NP，縦軸TF
        confusion_matrix = metrics.confusion_matrix(targets, d_test_predict)
        plt.figure(figsize=(10, 7))
        print("confusion_matrix : \n", confusion_matrix)
        # sns.heatmap(confusion_matrix, annot=True, fmt='.0f', cmap='Blues')
        # plt.show()

    def displayMistakes(self):
        """間違っている予測結果を表示"""
        for index,(p, t) in enumerate(zip(self.data_predict, self.targets_1d)):
            if p != t:
                k,i,j = self.indexes[index]
                label = self.names[k]
                print(label+" :")
                print("    predict : "+str(p))
                print("    answer  : "+str(t))
                center1 = [int(x+0.5) for x in self.features[k][i][:2]]
                center2 = [int(x+0.5) for x in self.features[k][j][:2]]
                
                img = cv2.imread(self.dataset_dir+"/images/"+label+".png")
                cv2.circle(img, center=center1, radius=5, thickness=2, color=(0,255,0), lineType=cv2.LINE_AA)
                cv2.circle(img, center=center2, radius=5, thickness=2, color=(0,255,0), lineType=cv2.LINE_AA)
                cv2.putText(img, text=label, org=(10,10), fontFace=cv2.FONT_HERSHEY_PLAIN,fontScale=1.5, color=(255,255,255),thickness=1,)
                cv2.imshow("img",img)
                if cv2.waitKey(0) == ord("q"):
                    break
        cv2.destroyAllWindows()


    def save(self, clf_file = None, scl_file = None):
        if clf_file == None:
            clf_file = self.clf_file
        if scl_file == None :
            scl_file = self.scl_file
        # モデルの保存
        with open(clf_file, mode="wb") as f:
            pickle.dump(self.clf,f)
        with open(scl_file, mode="wb") as f:
            pickle.dump(self.scl,f)
        

    def detect(self, img, max_bar_num=10): 
        """
        装甲板の中心座標と尤度を出力する

        Args 
            `img` : bgr画像
            `max_bar_num` : 考慮輪郭数の最大
        
        Return
            `centers` : 装甲板の中心座標\n
            `prob_values` : 装甲板である確率．0~1\n
            `decision_values` : 装甲板らしさ．0~1に正規化する前の値．負の値も取り得る．\n
        """
        # ぼかし
        img = GaussianBlur(img)
        # 2値化
        img = self.inRange(img)
        # 膨張
        img = dilate(img)
        # 輪郭抽出
        contours = list(cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0])
        if len(contours) == 0:
            return [],[],[]
        contours.sort(reverse=True, key=lambda x:cv2.contourArea(x))
        # 考慮輪郭数の最大
        mbn = min(max_bar_num, len(contours))
        # fitEllipse の結果を保存
        features = []
        for i in range(mbn):
            feature = fitEllipse(contours[i],0,0,img.shape[1])
            features.append(feature)
        # 結果の計算
        decision_values = []
        prob_values = []
        centers = []
        for i in range(mbn):
            cx1,cy1,*other1 = features[i]
            for j in range(i+1, mbn):
                cx2,cy2,*other2 = features[j]
                feature = [cx1-cx2, cy1-cy2, *other1, *other2]
                # print("f", feature)
                feature = self.scl.transform([feature])
                d = self.clf.decision_function(feature)[0]
                pr = self.clf.predict_proba(feature)[0][1]
                if d > 0:
                    decision_values.append(d)
                    prob_values.append(pr)
                    c = int((cx1+cx2)/2), int((cy1+cy2)/2)
                    centers.append(c)

        # decision_values はいらない可能性あり．
        centers = np.array(centers)
        decision_values = np.array(decision_values)
        prob_values = np.array(prob_values)
        return centers, decision_values, prob_values

    def detectAndCircle(self, img):
        """
        img に detect() を実行し，装甲板の中心座標に円を描画する．
        """
        centers,_,_ = self.detect(img)
        # detect 箇所の描画
        for i in range(len(centers)):
            c = centers[i]
            c = (int(c[0]+0.5), int(c[1]+0.5))
            cv2.circle(img, center=c, radius = 8, color = (0,255,0), thickness=5)







def main():
    # 入力
    enemy_color = "blue"
    dataset_dir = "/home/keitokohinata/test/detect-target-with-yolo-and-other-model/USBカメラ60FPS1280x720LowApt/blue-exp10-apt-min_mp4_cropped"
    debug = "bin" # bin or detect
    caution = True  

    # cbt および clf の準備
    cbt = ColorBarThreshold(
        enemy_color = enemy_color, 
        dataset_dir = dataset_dir,
    )

    clf = ArmorClassifier(
        inRange = cbt.inRange,
        # clf_file = "classifier/clf.pkl",
        # scl_file = "scaler/scl.pkl",
    )

    names, armors, bars, imgs, acrds, rcrds = cbt.prepare()
    cbt.fit()
    # cbt.save()
    # clf.prepare(dataset_dir, names, armors, bars, imgs, acrds, caution)

    # clf.fit()
    # clf.save()
    # clf.test()

    # 各画像に対してdetect
    img_fold = dataset_dir+"/images/"
    time_sum = 0
    count = 0
    imgs = sorted(os.listdir(img_fold))
    for img in imgs:
        img = cv2.imread(img_fold + img)
        if debug == "detect":
            # 実行時間の測定
            s = time()
            centers, decision_values, prob_values = clf.detect(img)
            e = time()
            time_sum += e-s
            count += 1
            # detect 箇所の描画
            for i in range(len(centers)):
                c = centers[i]
                c = (int(c[0]+0.5), int(c[1]+0.5))
                cv2.circle(img, center=c, radius = 10, color = (0,255,0), thickness=10)
        if debug == "bin":
            img = GaussianBlur(img)
            img = cbt.inRange(img)
            img = dilate(img)
        cv2.imshow("img", img)
        key = cv2.waitKey(0)
        if key == ord("q"):
            break
    cv2.destroyAllWindows()

    if debug == "detect":
        print("FPS : ", count/time_sum)



if __name__=="__main__":
    main()
